
var player1;
var player2;
//var keyboard;

var platforms_2 = [];

var leftWall_2;
var rightWall_2;
var ceiling_2;

var text1_2;
var text2_2;
var text3_2;

var distance_2 = 0;
var status = 'running';

var lastTime_2 = 0;

var gamebgm2;


var twoplayState = {
    preload:function(){

    },
    create:function(){
        
        game.add.image(0, 0, 'background_play');
        die_sound=game.add.audio('die');
        hurt_sound=game.add.audio('hurt');
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D
        });
    
        createBounders_2();//
        createPlayer_2();//
        createTextsBoard_2();//
    },
    update:function(){
        if(status == 'gameOver' && keyboard.enter.isDown) restart_2();
        if(status == 'gameOver' && keyboard.w.isDown) back();

        //if(status != 'running') return;
    
        this.physics.arcade.collide([player1, player2], platforms_2, effect);
        this.physics.arcade.collide([player1, player2], [leftWall_2, rightWall_2]);
        this.physics.arcade.collide(player1, player2);
        checkTouchCeiling(player1);
        checkTouchCeiling(player2);
        checkGameOver_2();//
    
        updatePlayer_2();//
        updatePlatforms_2();//
        updateTextsBoard_2();//
    
        createPlatforms_2();//
    }

};





function createBounders_2 () {
    gamebgm2=game.add.audio('gamebgm');
    gamebgm2.play();
    leftWall_2 = game.add.sprite(0, 0, 'wall');
    game.physics.arcade.enable(leftWall_2);
    leftWall_2.body.immovable = true;

    rightWall_2 = game.add.sprite(383, 0, 'wall');
    game.physics.arcade.enable(rightWall_2);
    rightWall_2.body.immovable = true;

    ceiling_2 = game.add.image(0, 0, 'ceiling');
}

function createPlatforms_2 () {
    if(game.time.now > lastTime_2 + 600) {
        lastTime_2 = game.time.now;
        createOnePlatform_2();
        distance_2 += 1;
    }
}

function createOnePlatform_2 () {

    var platform;
    var x = Math.random()*(400 - 96 - 40) + 20;
    var y = 400;
    var rand = Math.random() * 100;

    if(rand < 20) {
        platform = game.add.sprite(x, y, 'normal');
    } else if (rand < 40) {
        platform = game.add.sprite(x, y, 'nails');
        game.physics.arcade.enable(platform);
        platform.body.setSize(96, 15, 0, 15);
    } else if (rand < 50) {
        platform = game.add.sprite(x, y, 'conveyorLeft');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 60) {
        platform = game.add.sprite(x, y, 'conveyorRight');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 80) {
        platform = game.add.sprite(x, y, 'trampoline');
        platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
        platform.frame = 3;
    } else {
        platform = game.add.sprite(x, y, 'fake');
        platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
    }

    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms_2.push(platform);

    platform.body.checkCollision.down = false;
    platform.body.checkCollision.left = false;
    platform.body.checkCollision.right = false;
}

function createPlayer_2 () {
    player1 = game.add.sprite(300, 50, 'player');
    player2 = game.add.sprite(100, 50, 'player2');

    setPlayerAttr(player1);
    setPlayerAttr(player2);
}

function setPlayerAttr(player) {
    game.physics.arcade.enable(player);
    player.body.gravity.y = 500;
    player.animations.add('left', [0, 1, 2, 3], 8);
    player.animations.add('right', [9, 10, 11, 12], 8);
    player.animations.add('flyleft', [18, 19, 20, 21], 12);
    player.animations.add('flyright', [27, 28, 29, 30], 12);
    player.animations.add('fly', [36, 37, 38, 39], 12);
    player.life = 10;
    player.unbeatableTime = 0;
    player.touchOn = undefined;
}

function createTextsBoard_2 () {
    var style = {fill: '#ff0000', fontSize: '20px'}
    text1_2 = game.add.text(10, 10, '', style);
    text2_2 = game.add.text(350, 10, '', style);
    text3_2 = game.add.text(100, 150, '', style);
    text3_2.visible = false;
}

function updatePlayer_2 () {
    if(keyboard.left.isDown) {
        player1.body.velocity.x = -250;
    } else if(keyboard.right.isDown) {
        player1.body.velocity.x = 250;
    } else {
        player1.body.velocity.x = 0;
    }

    if(keyboard.a.isDown) {
        player2.body.velocity.x = -250;
    } else if(keyboard.d.isDown) {
        player2.body.velocity.x = 250;
    } else {
        player2.body.velocity.x = 0;
    }
    setPlayerAnimate(player1);
    setPlayerAnimate(player2);
}

function setPlayerAnimate(player) {
    var x = player.body.velocity.x;
    var y = player.body.velocity.y;

    if (x < 0 && y > 0) {
        player.animations.play('flyleft');
    }
    if (x > 0 && y > 0) {
        player.animations.play('flyright');
    }
    if (x < 0 && y == 0) {
        player.animations.play('left');
    }
    if (x > 0 && y == 0) {
        player.animations.play('right');
    }
    if (x == 0 && y != 0) {
        player.animations.play('fly');
    }
    if (x == 0 && y == 0) {
      player.frame = 8;
    }
}

function updatePlatforms_2 () {
    for(var i=0; i<platforms_2.length; i++) {
        var platform = platforms_2[i];
        platform.body.position.y -= 2;
        if(platform.body.position.y <= -20) {
            platform.destroy();
            platforms_2.splice(i, 1);
        }
    }
}

function updateTextsBoard_2 () {
    text1_2.setText('life:' + player1.life);
    text2_2.setText('life:' + player2.life);
}

function effect(player, platform) {
    if(platform.key == 'conveyorRight') {
        conveyorRightEffect(player, platform);
    }
    if(platform.key == 'conveyorLeft') {
        conveyorLeftEffect(player, platform);
    }
    if(platform.key == 'trampoline') {
        trampolineEffect(player, platform);
    }
    if(platform.key == 'nails') {
        nailsEffect(player, platform);
    }
    if(platform.key == 'normal') {
        basicEffect(player, platform);
    }
    if(platform.key == 'fake') {
        fakeEffect(player, platform);
    }
}

function conveyorRightEffect(player, platform) {
    player.body.x += 2;
}

function conveyorLeftEffect(player, platform) {
    player.body.x -= 2;
}

function trampolineEffect(player, platform) {
    platform.animations.play('jump');
    player.body.velocity.y = -350;
}

function nailsEffect(player, platform) {
    if (player.touchOn !== platform) {
        player.life -= 3;
        player.touchOn = platform;
        game.camera.flash(0xff0000, 100);
        hurt_sound.play();

    }
}

function basicEffect(player, platform) {
    if (player.touchOn !== platform) {
        if(player.life < 10) {
            player.life += 1;
        }
        player.touchOn = platform;
    }
}

function fakeEffect(player, platform) {
    if(player.touchOn !== platform) {
        platform.animations.play('turn');
        setTimeout(function() {
            platform.body.checkCollision.up = false;
        }, 100);
        player.touchOn = platform;
    }
}

function checkTouchCeiling(player) {
    if(player.body.y < 0) {
        if(player.body.velocity.y < 0) {
            player.body.velocity.y = 0;
        }
        if(game.time.now > player.unbeatableTime) {
            player.life -= 3;
            game.camera.flash(0xff0000, 100);
            player.unbeatableTime = game.time.now + 2000;
        }
    }
}

function checkGameOver_2 () {
    if(player1.life <= 0 || player1.body.y > 500) {
        gameOver_2('player2');
        //gamebgm2.destroy();
    }
    if(player2.life <= 0 || player2.body.y > 500) {
        gameOver_2('player1');
       // gamebgm2.destroy();
    }
}

function gameOver_2(winner) {
    text3_2.visible = true;
    text3_2.setText('WINNER: ' + winner + '\n\npress Enter to restart\n\npress W back to menu');
    platforms_2.forEach(function(s) {s.destroy()});
    platforms_2 = [];
    status = 'gameOver';
}

function restart_2 () {
    text3_2.visible = false;
    distance_2 = 0;
    createPlayer_2();
    //gamebgm2.play();//
    status = 'running';
}
function back(){
    gamebgm2.destroy();
    status = 'running';
    game.state.start('menu');
}

