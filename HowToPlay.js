
var HowToState={
    create: function() {
        game.add.image(0, 0, 'background_howto');

        game.add.text(10, 10, '此款小朋友下樓梯有2種模式\n按下PLAY進行單人模式\n按下TWO PLAYERS進行雙人模式\n\n單人模式由方向鍵左右控制人物\n\n雙人模式控制\nplayer1:方向鍵左右\nplayer2:A D', { font: '25px Arial', fill: '#ffffff' });
        


        this.menu= game.add.text(350,350,"Menu", { font: '25px Microsoft JhengHei', fill: '#ffffff' });
        this.menu.anchor.setTo(0.5, 0.5);
        this.menu.inputEnabled = true;        
        this.menu.events.onInputDown.add(this.down, this);
        this.menu.events.onInputOver.add(this.over, this);
        this.menu.events.onInputOut.add(this.out, this);
    },
    out:function(item){
        item.fill="#ffffff";
       
    },
     
    over:function(item)
    {
        item.fill = "#00bfff"; 
    },
    down:function(item) {
        game.state.start('menu');
    }
}
