
var menuState = {
    create: function() {
    // Add a background image
    game.add.image(0, 0, 'background');
    // Display the name of the game
    var nameLabel = game.add.text(game.width/2, 80, 'Go Down Stairs',
    { font: '50px Arial', fill: '#ffffff' });
    nameLabel.anchor.setTo(0.5, 0.5);
    game.add.tween(nameLabel).to({angle: -2}, 500).to({angle: 2}, 1000).to({angle: 0}, 500).loop().start();
    // Show the score at the center of the screen



    play_buttom = game.add.text(game.width/2, game.height/2, 'PLAY', { font: '25px Arial', fill: '#ffffff' });
    play_buttom.anchor.setTo(0.5, 0.5);
    play_buttom.inputEnabled = true;
    play_buttom.events.onInputDown.add(down, this);
    play_buttom.events.onInputOver.add(over, this);
    play_buttom.events.onInputOut.add(out, this);

    play2_buttom = game.add.text(game.width/2, game.height/2+50, 'TWO PLAYERS', { font: '25px Arial', fill: '#ffffff' });
    play2_buttom.anchor.setTo(0.5, 0.5);
    play2_buttom.inputEnabled = true;
    play2_buttom.events.onInputDown.add(down, this);
    play2_buttom.events.onInputOver.add(over, this);
    play2_buttom.events.onInputOut.add(out, this);

    board_buttom = game.add.text(game.width/2, game.height/2+100, 'ScoreBoard', { font: '25px Arial', fill: '#ffffff' });
    board_buttom.anchor.setTo(0.5, 0.5);
    board_buttom.inputEnabled = true;
    board_buttom.events.onInputDown.add(down, this);
    board_buttom.events.onInputOver.add(over, this);
    board_buttom.events.onInputOut.add(out, this);

    how_buttom = game.add.text(game.width/2, game.height/2+150, 'How to Play', { font: '25px Arial', fill: '#ffffff' });
    how_buttom.anchor.setTo(0.5, 0.5);
    how_buttom.inputEnabled = true;
    how_buttom.events.onInputDown.add(down, this);
    how_buttom.events.onInputOver.add(over, this);
    how_buttom.events.onInputOut.add(out, this);


    
    function out(item){
        item.fill="#ffffff";
       
    }
     
    function over(item)
    {
        item.fill = "#00bfff";
    }
    function down (item) {
        if(item.text=="PLAY")game.state.start('play');
        else if(item.text=="ScoreBoard") game.state.start('score');
        else if(item.text=="How to Play") game.state.start('howto');
        else if(item.text=="TWO PLAYERS") game.state.start('players');
        
    }
    
     }
};    