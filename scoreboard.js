var offset_top=50;
var num =1;
var scoreState={
    preload:function(){
        offset_top=50;
        num = 1;
    },
    create: function(){     
        game.add.image(0, 0, 'background_leader');
        
        var boardref=firebase.database().ref('scoreBoard');

            boardref.orderByChild('score').limitToFirst(10).on('child_added', function(snapshot){
            this.score=snapshot.val().score*(-1);
            this.name=snapshot.val().name;
            if(num < 4){
                game.add.text(50,offset_top,'NO.'+num,{ font: '20px Arial', fill: '#ffffff' });
            
                num = num + 1;
            }
            game.add.text(250,offset_top,this.name,{ font: '20px Arial', fill: '#ffffff' });
            game.add.text(150,offset_top,"B"+this.score,{ font: '20px Arial', fill: '#ffffff' });
            offset_top+=30;
            });
        this.menu= game.add.text(game.width-75,370,"Menu", { font: '25px Arial', fill: '#ffffff' });
        this.menu.anchor.setTo(0.5, 0.5);
        this.menu.inputEnabled = true;        
        this.menu.events.onInputDown.add(this.down, this);
        this.menu.events.onInputOver.add(this.over, this);
        this.menu.events.onInputOut.add(this.out, this);



    },
    out:function(item){
        item.fill="#ffffff";
       
    },
     
    over:function(item)
    {
        item.fill = "#00bfff"; 
    },
    down:function(item) {
        game.state.start('menu');
    }
    
}
