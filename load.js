var loadState = {
    preload: function () {
    // Add a 'loading...' label on the screen 

    var loadingLabel = game.add.text(game.width/2, 150,'loading...', { font: '30px Arial', fill: '#ffffff' });//
    loadingLabel.anchor.setTo(0.5, 0.5);
    // Display the progress bar
    var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');//
    progressBar.anchor.setTo(0.5, 0.5);
    game.load.setPreloadSprite(progressBar);

    //load sound
    game.load.audio('die','assets/die.wav');
    game.load.audio('hurt','assets/hurt.wav');
    game.load.audio('gamebgm', 'assets/Escape.wav');

    //load background
    game.load.image('background', 'assets/background.png');
    game.load.image('background_play','assets/background_play.png');
    game.load.image('background_howto','assets/howtobg.png');
    game.load.image('background_leader','assets/leaderbg.png');
    

    //load picture
    game.load.crossOrigin = 'anonymous';
    game.load.spritesheet('player', 'assets/player.png', 32, 32);
    game.load.spritesheet('player2', 'assets/player2.png', 32, 32);
    game.load.image('wall', 'assets/wall.png');
    game.load.image('ceiling', 'assets/ceiling.png');
    game.load.image('normal', 'assets/normal.png');
    game.load.image('nails', 'assets/nails.png');
    game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
    game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
    game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
    game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
},
    create: function() {
        // Go to the menu state
         game.state.start('menu');
    }, 
}; 